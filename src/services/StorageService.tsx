export const setStorage = (key: string, value: any) => {
  const encryptedVal: string = btoa(JSON.stringify(value));
  localStorage.setItem(key, encryptedVal);
};

export const getStorage = (key: string) => {
  const storedVal: any = localStorage.getItem(key);
  const decryptedVal: string = atob(storedVal);
  if (storedVal) return JSON.parse(decryptedVal);
  return null;
};

export const removeStorage = (key: string) => {
  localStorage.removeItem(key);
};
