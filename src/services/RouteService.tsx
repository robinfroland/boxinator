import React from "react";
import { Redirect } from "react-router";
import { AccountType } from "../constants/constants";

export const dashboardRouteSwitch = (role: AccountType) => {
  switch (role) {
    case AccountType.REGISTERED_USER:
      return <Redirect to="/" />;
    case AccountType.ADMINISTRATOR:
      return <Redirect to="/admin" />;
    case AccountType.GUEST:
      return <Redirect to="/guest" />;
  }
};
