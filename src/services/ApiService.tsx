import { AUTH_TOKEN_KEY, BASE_URL } from "../constants/constants";
import { getStorage } from "./StorageService";

export const optionsWithAuth = (
  requestMethod: string = "GET",
  requestBody?: any
) => {
  const requestOptions: RequestInit = {
    method: requestMethod,
    headers: {
      Authorization: `Bearer ${getStorage(AUTH_TOKEN_KEY)}`,
    },
  };
  if (
    requestBody !== undefined
    // requestMethod.toUpperCase() !== "GET" &&
    // requestMethod.toUpperCase() !== "DELETE"
  ) {
    requestOptions.body = requestBody;
    requestOptions.headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getStorage(AUTH_TOKEN_KEY)}`,
    };
  }
  return requestOptions;
};

export const fetchWithAuth = (endpoint: string) => {
  return fetch(BASE_URL + endpoint, optionsWithAuth()).then((response) =>
    response.json()
  );
};

export const postWithAuth = (endpoint: string, body: any) => {
  return fetch(
    BASE_URL + endpoint,
    optionsWithAuth("POST", JSON.stringify(body))
  ).then((response) => response.json());
};

export const putWithAuth = (endpoint: string, body: any) => {
  return fetch(
    BASE_URL + endpoint,
    optionsWithAuth("PUT", JSON.stringify(body))
  ).then((response) => response.json());
};

export const deleteWithAuth = (endpoint: string) => {
  return fetch(BASE_URL + endpoint, optionsWithAuth("DELETE"));
};
