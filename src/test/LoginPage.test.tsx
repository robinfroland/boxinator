import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import "mutationobserver-shim";
import { LoginForm } from "../components/login/LoginForm";
import { BrowserRouter } from "react-router-dom";
global.MutationObserver = window.MutationObserver;

describe("LoginPage", () => {
  test("TestForm", async () => {
    const mockOnLogin = jest.fn();
    render(
      <BrowserRouter>
        <LoginForm onLogin={mockOnLogin} />
      </BrowserRouter>
    );
    const emailInput = screen.getByTestId("emailLogin");
    const passWordInput = screen.getByTestId("passwordLogin");

    userEvent.type(emailInput, "magne@test.com");
    userEvent.type(passWordInput, "Password21@");

    userEvent.click(screen.getByRole("button", { name: /Login/i }));
    await waitFor(() => {
      expect(mockOnLogin).toHaveBeenCalledTimes(1);
    });
  });
});
