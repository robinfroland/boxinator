import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import { RegisterForm } from "../components/register/RegisterForm";
import userEvent from "@testing-library/user-event";

import "mutationobserver-shim";
global.MutationObserver = window.MutationObserver;

describe("RegisterPage", () => {
  test("TestEmailInvalidInput", async () => {
    const mocOnRegistation = jest.fn();
    render(<RegisterForm onRegistration={mocOnRegistation} />);
    const emailInput = screen.getByLabelText("E-mail address*");
    expect(emailInput).toBeInTheDocument();

    userEvent.type(emailInput, "test.com");
    userEvent.click(screen.getByRole("button", { name: /register/i }));
    await waitFor(() => {
      expect(emailInput).not.toBeValid();
    });
  });

  test("TestEmailValidInput", async () => {
    const mockOnRegistation = jest.fn();
    render(<RegisterForm onRegistration={mockOnRegistation} />);
    const emailInput = screen.getByLabelText("E-mail address*");
    expect(emailInput).toBeInTheDocument();

    userEvent.type(emailInput, "test@works.com");
    userEvent.click(screen.getByRole("button", { name: /register/i }));
    await waitFor(() => {
      expect(emailInput).toBeValid();
    });
  });

  test("TestPasswordInvalidInput", async () => {
    const mockOnRegistation = jest.fn();
    render(<RegisterForm onRegistration={mockOnRegistation} />);
    const passwordInput = screen.getByLabelText("Password*");
    expect(passwordInput).toBeInTheDocument();

    userEvent.type(passwordInput, "password123");
    userEvent.click(screen.getByRole("button", { name: /register/i }));
    await waitFor(() => {
      expect(passwordInput).not.toBeValid();
    });
  });

  test("TestPasswordValidInput", async () => {
    const mockOnRegistation = jest.fn();
    render(<RegisterForm onRegistration={mockOnRegistation} />);
    const passwordInput = screen.getByLabelText("Password*");
    expect(passwordInput).toBeInTheDocument();

    userEvent.type(passwordInput, "Password123@");
    userEvent.click(screen.getByRole("button", { name: /register/i }));
    await waitFor(() => {
      expect(passwordInput).toBeValid();
    });
  });

  test("TestForm", async () => {
    const mockOnRegistation = jest.fn();
    render(<RegisterForm onRegistration={mockOnRegistation} />);
    const emailInput = screen.getByLabelText("E-mail address*");
    const passWordInput = screen.getByLabelText("Password*");
    const validatePasswordInput = screen.getByLabelText("Confirm password*");
    const firstNameInput = screen.getByLabelText("First name*");
    const lastNameInput = screen.getByLabelText("Last name*");

    userEvent.type(emailInput, "magne@test.com");
    userEvent.type(passWordInput, "Password21@");
    userEvent.type(validatePasswordInput, "Password21@");
    userEvent.type(firstNameInput, "Magne");
    userEvent.type(lastNameInput, "Kjellesvik");

    userEvent.click(screen.getByRole("button", { name: /register/i }));
    await waitFor(() => {
      expect(mockOnRegistation).toHaveBeenCalledTimes(1);
    });
  });
});
