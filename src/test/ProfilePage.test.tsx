import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { ProfileForm } from "../components/profile/ProfileForm";
import { AccountType } from "../constants/constants";
import { IAccount } from "../constants/interfaces";

import "mutationobserver-shim";
global.MutationObserver = window.MutationObserver;

describe("ProfilePage", () => {
  const mockAccount = {
    id: 123,
    role: AccountType.REGISTERED_USER,
    firstName: "test",
    lastName: "string",
    email: "test@string.no",
    password: "Password21@",
    phone: "90808090",
    zipCode: "0330",
    dob: "12/02/20",
    country: "Notway",
  };

  test("TestPasswordValidInput", async () => {
    render(
      <ProfileForm
        updateProfile={(profile: IAccount) => {
          return null;
        }}
        account={mockAccount}
      />
    );
    const passwordInput = screen.getByLabelText("Password");
    expect(passwordInput).toBeInTheDocument();

    userEvent.click(screen.getByTestId("EditBtn"));
    userEvent.type(passwordInput, "Password3@!");
    userEvent.click(screen.getByRole("button", { name: /Save Changes/i }));
    await waitFor(() => {
      expect(passwordInput).toBeValid();
    });
  });

  test("TestPasswordInvalidInput", async () => {
    render(
      <ProfileForm
        updateProfile={(profile: IAccount) => {
          return null;
        }}
        account={mockAccount}
      />
    );
    const passwordInput = screen.getByLabelText("Password");
    expect(passwordInput).toBeInTheDocument();

    userEvent.click(screen.getByTestId("EditBtn"));
    userEvent.type(passwordInput, "passord");
    userEvent.click(screen.getByRole("button", { name: /Save Changes/i }));
    await waitFor(() => {
      expect(passwordInput).not.toBeValid();
    });
  });

  test("TestConfirmPasswordTrue", async () => {
    render(
      <ProfileForm
        updateProfile={(profile: IAccount) => {
          return null;
        }}
        account={mockAccount}
      />
    );
    const passwordInput = screen.getByLabelText("Password");
    expect(passwordInput).toBeInTheDocument();

    userEvent.click(screen.getByTestId("EditBtn"));
    const confirmPasswordInput = screen.getByLabelText("Confirm password");
    userEvent.type(passwordInput, "Password21$$");
    userEvent.type(confirmPasswordInput, "Password21$$");
    userEvent.click(screen.getByRole("button", { name: /Save Changes/i }));
    await waitFor(() => {
      expect(confirmPasswordInput).toBeValid();
    });
  });

  test("TestConfirmPasswordFalse", async () => {
    render(
      <ProfileForm
        updateProfile={(profile: IAccount) => {
          return null;
        }}
        account={mockAccount}
      />
    );
    const passwordInput = screen.getByLabelText("Password");
    expect(passwordInput).toBeInTheDocument();

    userEvent.click(screen.getByTestId("EditBtn"));
    const confirmPasswordInput = screen.getByLabelText("Confirm password");
    userEvent.type(passwordInput, "Password21$$");
    userEvent.type(confirmPasswordInput, "Password!!12");
    userEvent.click(screen.getByRole("button", { name: /Save Changes/i }));
    await waitFor(() => {
      expect(confirmPasswordInput).not.toBeValid();
    });
  });

  test("TestForm", async () => {
    const mockOnRegistation = jest.fn();
    //render(<ProfileForm onSubmit={mockOnRegistation} />);
    render(
      <ProfileForm
        updateProfile={(profile: IAccount) => {
          return null;
        }}
        account={mockAccount}
      />
    );
    const passwordInput = screen.getByLabelText("Password");
    const firstNameInput = screen.getByLabelText("First name");
    const lastNameInput = screen.getByLabelText("Last name");

    userEvent.click(screen.getByTestId("EditBtn"));
    const validatePasswordInput = screen.getByLabelText("Confirm password");
    userEvent.type(passwordInput, mockAccount.password);
    userEvent.type(validatePasswordInput, mockAccount.password);
    userEvent.type(firstNameInput, mockAccount.firstName);
    userEvent.type(lastNameInput, mockAccount.lastName);

    userEvent.click(screen.getByRole("button", { name: /Save Changes/i }));
    await waitFor(() => {
      //expect(mockOnRegistation).toHaveBeenCalledTimes(1);
      expect(passwordInput).toBeValid();
      expect(validatePasswordInput).toBeValid();
      expect(firstNameInput).toBeValid();
      expect(lastNameInput).toBeValid();
    });
  });
});
