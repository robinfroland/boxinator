import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import { AddShipmentForm } from "../components/dashboard/AddShipmentForm";
import { AuthStateContext } from "../context/context";
import { AuthDispatchContext } from "../context/context";
import { AccountType } from "../constants/constants";

import "mutationobserver-shim";
import userEvent from "@testing-library/user-event";
import { IShipment } from "../constants/interfaces";
global.MutationObserver = window.MutationObserver;

describe("AddShipmentForm", () => {
  const mockCountries = [
    { id: "123", name: "Norway", feeMultiplier: "1.1" },
    { id: "321", name: "Sweden", feeMultiplier: "1.2" },
  ];

  const mockAccount = {
    isAuthenticated: true,
    role: AccountType.REGISTERED_USER,
    name: "Test user",
    loading: false,
    loginError: null,
    availableCountries: mockCountries,
  };

  render(
    <AuthStateContext.Provider value={mockAccount}>
      <AuthDispatchContext.Provider value={jest.fn()}>
        <div id="root">
          <AddShipmentForm
            updateShipments={(shipment: IShipment) => {
              return null;
            }}
            setColor={(color: any) => {
              return null;
            }}
          />
        </div>
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );

  const receiverInput = screen.getByTestId("receiverName");
  const weightInput = screen.getByTestId("weight");
  const countryInput = screen.getByTestId("country");
  const addShipmentButton = screen.getByTestId("shipBtn");

  test("renderTest", () => {
    expect(receiverInput).toBeInTheDocument();
    expect(weightInput).toBeInTheDocument();
    expect(countryInput).toBeInTheDocument();
    expect(addShipmentButton).toBeInTheDocument();
  });

  test("testValidReceiverInput", async () => {
    userEvent.type(receiverInput, "Valid Name");
    userEvent.type(weightInput, 1.2);
    userEvent.click(addShipmentButton);
    await waitFor(() => {
      expect(receiverInput).toBeValid();
      expect(weightInput).toBeValid();
    });
  });

  /*
  //  Error in test, not valid is not working
  test("testTooLongReceiverName", async () => {
    userEvent.type(
      receiverInput,
      "IIIIIIINVALIDNAMEBECAUSEITISTOOOOOOOOOOOLOOOOOOOOOOONG"
    );
    userEvent.click(addShipmentButton);
    await waitFor(() => {
      expect(receiverInput).not.toBeValid();
    });
  });

  //  Error in test, not valid is not working
  test("testInvalidReceiverInput", async () => {
    userEvent.type(receiverInput, "£@£€£@£");
    userEvent.type(weightInput, "-0.1");
    userEvent.click(addShipmentButton);
    await waitFor(() => {
      expect(receiverInput).not.toBeValid();
      expect(weightInput).not.toBeValid();
    });
  });
  */
});
