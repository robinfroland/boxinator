import React from "react";
import { render, screen } from "@testing-library/react";
import { PageNotFound } from "../components/common/PageNotFound";
import { BrowserRouter } from "react-router-dom";

import "mutationobserver-shim";
global.MutationObserver = window.MutationObserver;

describe("PageNotFound", () => {
  test("renderTest", () => {
    render(
      <BrowserRouter>
        <PageNotFound />
      </BrowserRouter>
    );
    const heading = screen.getByRole("heading", { name: "404" });
    expect(heading).toBeInTheDocument();
    expect(
      screen.getByRole("heading", { name: /Not Found/i })
    ).toBeInTheDocument();
  });
});
