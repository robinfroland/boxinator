import React from "react";
import ReactDOM from "react-dom";
import "./style/style.scss";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { AdminProtectedRoute } from "./components/routes/AdminProtectedRoute";
import { UserProtectedRoute } from "./components/routes/UserProtectedRoute";
import { GuestProtectedRoute } from "./components/routes/GuestProtectedRoute";
import { ProtectedRoute } from "./components/routes/ProtectedRoute";
import { AppStateProvider } from "./context/context";

import { Navbar } from "./components/common/Navbar";
import { RegisterPage } from "./components/register/RegisterPage";
import { LoginPage } from "./components/login/LoginPage";
import { DashboardPage } from "./components/dashboard/user/DashboardPage";
import { GuestDashboardPage } from "./components/dashboard/guest/GuestDashboardPage";
import { AdminDashboardPage } from "./components/dashboard/admin/AdminDashboardPage";
import { ProfilePage } from "./components/profile/ProfilePage";
import { VerifyPage } from "./components/register/VerifyPage";
import { PageNotFound } from "./components/common/PageNotFound";
import { RegisterSuccessPage } from "./components/register/RegisterSuccessPage";

function App() {
  return (
    <Router>
      <AppStateProvider>
        <Navbar />
        <Switch>
          <UserProtectedRoute exact path="/" component={DashboardPage} />
          <AdminProtectedRoute
            exact
            path="/admin"
            component={AdminDashboardPage}
          />
          <GuestProtectedRoute
            exact
            path="/guest"
            component={GuestDashboardPage}
          />
          <ProtectedRoute exact path="/profile" component={ProfilePage} />
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/register" component={RegisterPage} />
          <Route exact path="/verify" component={VerifyPage} />
          <Route
            exact
            path="/register/success"
            component={RegisterSuccessPage}
          />
          <Route path="*" component={PageNotFound} />
        </Switch>
      </AppStateProvider>
    </Router>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Router>
    <App />
  </Router>,
  rootElement
);

export default App;
