import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { AccountType } from "../../constants/constants";
import { login, validateTwoFA } from "../../context/actions";
import { useAppDispatch, useAppState } from "../../context/context";
import { LoginForm } from "./LoginForm";
import Modal from "react-modal";
import { BASE_URL } from "../../constants/constants";
import { useForm } from "react-hook-form";
import { IAccount } from "../../constants/interfaces";
import { ReactComponent as CloseButton } from "../../assets/close_icon.svg";

/*
 * This components contains several functions mainly handling different roles.
 * The roles Guest/User/Admin all gets redirected to different dashboards.
 * If you choose to continue as a guest, a modal will be displayed.
 * Several checks are being done on the user input on the login
 * as well as some checks to the database.
 */
export const LoginPage: React.FC = () => {
  const { register, handleSubmit, errors } = useForm<IAccount>();
  const { register: reg, handleSubmit: handle, errors: err } = useForm<any>();
  const [guestModalIsOpen, setGuestModalIsOpen] = useState(false);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const history = useHistory();
  const dispatch = useAppDispatch();
  const { loginError, role, isAuthenticated } = useAppState();
  const [twoFA, setTwoFA] = useState({ email: "", code: "" });
  const [validTwoFA, setValidTwoFA] = useState(null);
  const [token, setToken] = useState("");

  useEffect(() => {
    if (isAuthenticated) {
      handleRedirect(role);
    }
  });

  const handleLogin = async (email: string, password?: string) => {
    setTwoFA({ ...twoFA, email: email });
    const { role, token } = password
      ? await login(dispatch, email, password)
      : await login(dispatch, email);
    setToken(token);
    if (role === AccountType.GUEST) {
      handleRedirect(role);
    } else if (token) {
      setModalIsOpen(true);
    }
  };

  useEffect(() => {
    Modal.setAppElement("#root");
  }, []);

  const handleTwoFA = async () => {
    const valid = await validateTwoFA(dispatch, twoFA, token);
    setValidTwoFA(valid);
    if (valid) handleRedirect(role);
  };

  const handleGuestLogin = async (data: IAccount) => {
    registerGuestAccount(data);
    handleLogin(data.email);
    setGuestModalIsOpen(false);
  };

  const handleRedirect = (role: AccountType) => {
    if (role === AccountType.REGISTERED_USER) {
      history.push("/");
    } else if (role === AccountType.ADMINISTRATOR) {
      history.push("/admin");
    } else if (role === AccountType.GUEST) {
      history.push("/guest");
    }
  };

  const registerGuestAccount = async (data: IAccount) => {
    data.role = AccountType.GUEST;
    await fetch(BASE_URL + "/account", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
  };

  return (
    <div className="container d-flex flex-column align-items-center top-container">
      <h1 className="mb-2">Log in to Boxinator</h1>
      <small
        className="mb-4 small-link"
        onClick={() => setGuestModalIsOpen(true)}
      >
        Or click here to continue as a guest
      </small>
      {loginError === 401 && (
        <p className="text-danger">
          Username or password is incorrect. Please try again.
        </p>
      )}
      {loginError === 403 && (
        <p className="text-danger">
          Your user is not activated yet. Please check your email.
        </p>
      )}
      <LoginForm onLogin={handleLogin} />

      <Modal
        className="Modal"
        overlayClassName="Overlay"
        isOpen={modalIsOpen}
        onRequestClose={() => setModalIsOpen(false)}
      >
        <div className="top d-flex justify-content-end">
          <CloseButton onClick={() => setModalIsOpen(false)} />
        </div>
        <div className="content">
          <div className="header">
            <h4>Two-factor Authentication</h4>
            <p>Enter the code from your authenticator app</p>
          </div>

          <div className="d-flex flex-column align-items-center text-center">
            <form onSubmit={handle(handleTwoFA)}>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter code"
                  id="code"
                  name="code"
                  onChange={(e) => {
                    setTwoFA({ ...twoFA, code: e.target.value.trim() });
                  }}
                  ref={reg({
                    required: true,
                    pattern: /^(\d)*$/,
                  })}
                />
                {err.code?.type === "required" && (
                  <p className="invalid">2FA is required</p>
                )}
                {err.code?.type === "pattern" && (
                  <p className="invalid">2FA-code can only contain digits</p>
                )}
                {validTwoFA === false && (
                  <p className="text-danger">
                    Invalid 2FA-code. Please try again.
                  </p>
                )}
              </div>
              <button type="submit" className="btn btn-success">
                Continue
              </button>
            </form>
          </div>
        </div>
      </Modal>

      <Modal
        className="Modal"
        overlayClassName="Overlay"
        isOpen={guestModalIsOpen}
        onRequestClose={() => setGuestModalIsOpen(false)}
      >
        <div className="top d-flex justify-content-end">
          <CloseButton onClick={() => setGuestModalIsOpen(false)} />
        </div>
        <div className="content">
          <div className="header">
            <h4>Enter your email to continue</h4>
            <p>
              We will send a receipt to this email when you create a new
              shipment.
              <br />
              We'll never share your email with anyone else.
            </p>
          </div>
          <div className="d-flex flex-column align-items-center text-center">
            <form onSubmit={handleSubmit(handleGuestLogin)}>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  name="email"
                  placeholder="Please enter your e-mail"
                  ref={register({
                    required: true,
                    // Regex: General Email Regex (RFC 5322 Official Standard)
                    pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                  })}
                />
                {errors.email?.type === "required" && (
                  <p className="invalid">
                    E-mail is required to continue as a guest
                  </p>
                )}
                {errors.email?.type === "pattern" && (
                  <p className="invalid">Please enter a valid email address</p>
                )}
              </div>

              <button type="submit" className="btn btn-primary mt-1">
                Continue as guest
              </button>
            </form>
          </div>
        </div>
      </Modal>
    </div>
  );
};
