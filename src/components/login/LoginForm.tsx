import React, { useState } from "react";
import { Link } from "react-router-dom";

interface Props {
  onLogin: (email: string, password: string) => void;
}

/*
 * This is a simple Login form that contains two input fields:
 * one for email and one for password, and a button.
 * There will also be a link to follow if you do not have an account.
 */
export const LoginForm: React.FC<Props> = ({ onLogin }) => {
  const [credentials, setCredentials] = useState({ email: "", password: "" });

  return (
    <form>
      <div className="form-group">
        <input
          type="email"
          className="form-control form-control-lg"
          id="email-input"
          placeholder="E-mail"
          data-testid="emailLogin"
          aria-describedby="email"
          onChange={(e) => {
            setCredentials({ ...credentials, email: e.target.value });
          }}
        />
      </div>
      <div className="form-group">
        <input
          type="password"
          className="form-control form-control-lg"
          placeholder="Password"
          data-testid="passwordLogin"
          id="password-input"
          onChange={(e) => {
            setCredentials({ ...credentials, password: e.target.value });
          }}
          onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
            if (event.key === "Enter") {
              onLogin(credentials.email, credentials.password);
            }
          }}
        />
      </div>
      <div className="text-center d-flex flex-column">
        <button
          type="button"
          className="btn btn-primary btn-lg"
          disabled={false}
          onClick={() => onLogin(credentials.email, credentials.password)}
        >
          Login
        </button>
        <small className="mt-3">
          Don't have an account?
          <Link to="/register"> Click here to register</Link>
        </small>
      </div>
    </form>
  );
};
