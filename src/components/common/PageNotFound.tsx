import React from "react";
import { Link } from "react-router-dom";

/*
 * A 404 not Found page that is being used to all paths
 * that doesn't have a component.
 */
export const PageNotFound: React.FC = () => {
  return (
    <div className="container text-center top-container">
      <h1>404</h1>
      <h2>Page Not Found</h2>
      <p>The page you are looking for does not exist!</p>
      <Link to="/">Click here to go back</Link>
    </div>
  );
};
