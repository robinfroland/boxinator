import React from "react";
import { Link } from "react-router-dom";
import { ReactComponent as ProfileIcon } from "../../assets/profile_icon.svg";
import { AccountType } from "../../constants/constants";
import { useAppState } from "../../context/context";

/*
 * A common navbar made to be re-used in all the components
 * It contains a logo as a button to the homepage and another button
 * as a profileIcon and the actual users name
 */
export const Navbar = () => {
  let { isAuthenticated, name, role } = useAppState();
  return (
    <nav className="navbar space-between">
      <Link to="/" className="nav-logo">
        BOXINATOR
      </Link>
      {(isAuthenticated || role === AccountType.GUEST) && (
        <Link to="/profile" className="nav-profile">
          <p className="m-0 mr-1 profile-name d-none d-sm-inline">{name}</p>
          <ProfileIcon className="btn-icon profile-icon" />
        </Link>
      )}
    </nav>
  );
};
