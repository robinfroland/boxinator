import React, { useEffect, useRef, useState } from "react";
import { ShipmentList } from "../ShipmentList";
import { AddShipmentForm } from "../AddShipmentForm";
import { PackageIllustration } from "./PackageIllustration";
import {
  fetchWithAuth,
  postWithAuth,
  putWithAuth,
} from "../../../services/ApiService";
import { IShipment } from "../../../constants/interfaces";
import { ShipmentStatus } from "../../../constants/constants";
import { Alert } from "react-bootstrap";
import { ReactComponent as CloseButton } from "../../../assets/close_icon.svg";
import Modal from "react-modal";

/*
 * This is the dashboard that is being displayed for all the registered users.
 * Here you have the opportunity to add new shipments with the AddShipmentForm.
 * A Registered user will also get to see any active/previous shipments made.
 * A user can also cancel/delete their own shipments.
 */
export const DashboardPage: React.FC = () => {
  const [color, setColor] = useState<string>("#6610f2");
  const [shipments, setShipments] = useState<any[]>([]);
  const [completedShipments, setCompletedShipments] = useState<any[]>([]);
  const [cancelledShipments, setCancelledShipments] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [showAddAlert, setShowAddAlert] = useState(false);
  const [showCancelAlert, setShowCancelAlert] = useState(false);
  const isMounted = useRef<boolean>(true);
  const [cancelledShipment, setCancelledShipment] = useState<IShipment | null>(
    null
  );

  useEffect(() => {
    Modal.setAppElement("#root");
  }, []);

  useEffect(() => {
    Promise.all([
      fetchWithAuth("/shipments?sort=date"),
      fetchWithAuth("/shipments/complete?sort=date"),
      fetchWithAuth("/shipments/cancelled?sort=date"),
    ]).then((response) => {
      if (isMounted.current) {
        setShipments(response[0]);
        setCompletedShipments(response[1]);
        setCancelledShipments(response[2]);
        setLoading(false);
      }
    });
    return () => {
      isMounted.current = false;
    };
  }, []);

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const cancelShipmentModal = (cancelledShipment: IShipment) => {
    setModalIsOpen(true);
    setCancelledShipment(cancelledShipment);
  };

  const cancelShipment = async () => {
    await putWithAuth(`/shipments/${cancelledShipment?.id}`, {
      id: cancelledShipment?.id,
      status: ShipmentStatus.CANCELLED,
      account: { id: cancelledShipment?.account?.id },
    });
    setShipments(
      shipments.filter((shipment) => shipment.id !== cancelledShipment?.id)
    );
    setCancelledShipments([
      { ...cancelledShipment, status: ShipmentStatus.CANCELLED },
      ...cancelledShipments,
    ]);
    setShowAddAlert(false);
    setShowCancelAlert(true);
    setModalIsOpen(false);
  };

  const addShipment = async (shipment: IShipment) => {
    await postWithAuth("/shipments", shipment).then((response) =>
      setShipments([response, ...shipments])
    );
    setShowCancelAlert(false);
    setShowAddAlert(true);
  };

  return (
    <>
      <div className="container d-flex justify-content-center">
        <Alert
          show={showAddAlert}
          variant="success"
          className="text-center position-absolute"
          onClose={() => setShowAddAlert(false)}
          dismissible
        >
          Shipment successfully added!
        </Alert>
        <Alert
          show={showCancelAlert}
          variant="success"
          className="text-center position-absolute"
          onClose={() => setShowCancelAlert(false)}
          dismissible
        >
          Shipment successfully cancelled!
        </Alert>
      </div>
      <div className="dashboard">
        <section className="container add-shipment">
          <div className="row">
            <div className="col">
              <h3>Add new shipment</h3>
              <AddShipmentForm
                updateShipments={addShipment}
                setColor={setColor}
              />
            </div>
            <div className="d-none d-md-flex col-md-7 justify-content-center">
              <PackageIllustration color={color} />
            </div>
          </div>
        </section>
        {loading ? (
          <p className="text-center mt-">Currently loading shipments...</p>
        ) : (
          <div className="shipment-history">
            <h5>Active shipments</h5>
            {shipments.length < 1 ? (
              <h6 className="text-center mt-5">You have no active shipments</h6>
            ) : (
              <ShipmentList
                updateShipments={cancelShipmentModal}
                shipments={shipments}
              />
            )}
            <h5>Previous shipments</h5>
            {completedShipments.length === 0 &&
            cancelledShipments.length === 0 ? (
              <h6 className="text-center mt-5">
                You have no previous shipments
              </h6>
            ) : (
              <ShipmentList
                shipments={[...cancelledShipments, ...completedShipments]}
              />
            )}
          </div>
        )}
      </div>

      <Modal
        className="Modal"
        overlayClassName="Overlay"
        isOpen={modalIsOpen}
        onRequestClose={() => setModalIsOpen(false)}
      >
        <div className="top d-flex justify-content-end">
          <CloseButton onClick={() => setModalIsOpen(false)} />
        </div>
        <div className="content">
          <div className="header">
            <h4>Cancel shipment</h4>
            <p>Are you sure you want to cancel the shipment?</p>
          </div>
        </div>
        <div className="d-flex justify-content-center">
          <button
            type="submit"
            className="btn btn-danger"
            onClick={() => cancelShipment()}
          >
            Cancel shipment
          </button>
          <button
            type="submit"
            className="btn btn-secondary ml-2"
            onClick={() => setModalIsOpen(false)}
          >
            Go back
          </button>
        </div>
      </Modal>
    </>
  );
};
