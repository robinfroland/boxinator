import React, { ChangeEvent } from "react";
import { ShipmentStatus } from "../../../constants/constants";
import { IShipment } from "../../../constants/interfaces";

interface Props {
  status: string;
  shipment: IShipment;
  updateStatus?: (shipment: IShipment) => void;
}

/*
 * The dropdown Component used on the Admin shipmentList.
 */

export const StatusDropdown: React.FC<Props> = ({
  status,
  shipment,
  updateStatus,
}) => {
  return (
    <>
      <select
        className="form-control select-small d-inline"
        defaultValue={status}
        onChange={(event: ChangeEvent<HTMLSelectElement>) => {
          updateStatus &&
            updateStatus({
              ...shipment,
              status:
                event.target.value === "IN TRANSIT"
                  ? ShipmentStatus.IN_TRANSIT
                  : event.target.value,
            });
        }}
      >
        {Object.keys(ShipmentStatus).map((status) => (
          <option key={status}>
            {status === ShipmentStatus.IN_TRANSIT
              ? status.replace("_", " ")
              : status}
          </option>
        ))}
      </select>
    </>
  );
};
