import React, { useEffect, useRef, useState } from "react";
import {
  deleteWithAuth,
  fetchWithAuth,
  putWithAuth,
} from "../../../services/ApiService";
import { ShipmentList } from "../ShipmentList";
import { UpdateFeeForm } from "./UpdateFeeForm";
import { ReactComponent as MapIllustration } from "../../../assets/world_map.svg";
import { ICountry, IShipment } from "../../../constants/interfaces";
import { Alert } from "react-bootstrap";
import { useAppState } from "../../../context/context";
import { setStorage } from "../../../services/StorageService";
import { COUNTRIES_KEY } from "../../../constants/constants";
import { ReactComponent as CloseButton } from "../../../assets/close_icon.svg";
import Modal from "react-modal";

/*
 * Admin dashboard has some more features than the other dashboards.
 * The component contains a form for editing Country fees.
 * It also contains a list of all the shipments, where admin has a
 * dropdown menu to change the status of each shipment.
 */
export const AdminDashboardPage: React.FC = () => {
  const { availableCountries } = useAppState();
  const [shipments, setShipments] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const isMounted = useRef<boolean>(true);
  const [showAlert, setShowAlert] = useState(false);
  const [showStatusAlert, setShowStatusAlert] = useState(false);
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);
  const [deletedShipment, setDeletedShipment] = useState<IShipment | null>(
    null
  );
  const [modalIsOpen, setModalIsOpen] = useState(false);

  useEffect(() => {
    Modal.setAppElement("#root");
  }, []);

  useEffect(() => {
    fetchWithAuth("/shipments?sort=status")
      .then((response) => {
        if (isMounted.current) {
          setShipments(response);
          setLoading(false);
        }
      })
      .catch((error) => console.log(error));
    return () => {
      isMounted.current = false;
    };
  }, []);

  const updateFee = async (country: ICountry) => {
    await putWithAuth(`/settings/countries/${country.id}`, country);
    setShowAlert(true);
    const countryIndex = availableCountries.findIndex(
      (c: ICountry) => c.id === country.id
    );
    availableCountries[countryIndex] = {
      ...availableCountries[countryIndex],
      feeMultiplier: country.feeMultiplier,
    };
    setStorage(COUNTRIES_KEY, availableCountries);
  };

  const deleteShipmentModal = (deletedShipment: IShipment) => {
    setModalIsOpen(true);
    setDeletedShipment(deletedShipment);
  };

  const deleteShipment = () => {
    deleteWithAuth(`/shipments/${deletedShipment?.id}`);
    setShipments(
      shipments.filter((shipment) => shipment.id !== deletedShipment?.id)
    );
    setShowDeleteAlert(true);
    setModalIsOpen(false);
  };

  const updateShipmentStatus = async (shipment: IShipment) => {
    await putWithAuth(`/shipments/${shipment.id}`, {
      id: shipment.id,
      status: shipment.status,
    });
    setShowStatusAlert(true);

    // Display success/error message
  };

  return (
    <>
      <div className="container d-flex justify-content-center">
        <Alert
          show={showAlert}
          variant="success"
          className="text-center position-absolute"
          onClose={() => setShowAlert(false)}
          dismissible
        >
          Fee successfully updated!
        </Alert>
      </div>
      <div className="dashboard">
        <section className="container add-shipment">
          <div className="row">
            <div className="col">
              <h3>Update fees for...</h3>
              <UpdateFeeForm updateFee={updateFee} />
            </div>
            <div className="d-none d-md-flex col-md-7 justify-content-center">
              <MapIllustration />
            </div>
          </div>
        </section>
        <div className="container d-flex justify-content-center">
          <Alert
            show={showStatusAlert}
            variant="success"
            className="text-center position-absolute"
            onClose={() => setShowStatusAlert(false)}
            dismissible
          >
            Shipment status successfully updated!
          </Alert>
        </div>
        <div className="container d-flex justify-content-center">
          <Alert
            show={showDeleteAlert}
            variant="success"
            className="text-center position-absolute"
            onClose={() => setShowDeleteAlert(false)}
            dismissible
          >
            Shipment successfully deleted!
          </Alert>
        </div>

        {loading ? (
          <p className="text-center mt-5">Currently loading shipments...</p>
        ) : (
          <div className="shipment-history">
            <h5>Active shipments</h5>
            <ShipmentList
              updateShipments={deleteShipmentModal}
              updateStatus={updateShipmentStatus}
              shipments={shipments}
            />
          </div>
        )}
      </div>
      <Modal
        className="Modal"
        overlayClassName="Overlay"
        isOpen={modalIsOpen}
        onRequestClose={() => setModalIsOpen(false)}
      >
        <div className="top d-flex justify-content-end">
          <CloseButton onClick={() => setModalIsOpen(false)} />
        </div>
        <div className="content">
          <div className="header">
            <h4>Delete shipment</h4>
            <p>Are you sure you want to delete the shipment?</p>
          </div>
        </div>
        <div className="d-flex justify-content-center">
          <button
            type="submit"
            className="btn btn-danger"
            onClick={() => deleteShipment()}
          >
            Delete
          </button>
          <button
            type="submit"
            className="btn btn-secondary ml-2"
            onClick={() => setModalIsOpen(false)}
          >
            Cancel
          </button>
        </div>
      </Modal>
    </>
  );
};
