import React, { ChangeEvent, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { ICountry } from "../../../constants/interfaces";

import { useAppState } from "../../../context/context";
import { fetchWithAuth } from "../../../services/ApiService";

interface Props {
  updateFee: (data: ICountry) => void;
}
/*
 * The updateFeeForm is a short form that takes in a country from availableCountries.
 * After selecting country it displays this country and the actual fee.
 * You can here change the fee from 10-100
 */
export const UpdateFeeForm: React.FC<Props> = ({ updateFee }) => {
  const { availableCountries } = useAppState();
  const [selectedCountry, setSelectedCountry] = useState<ICountry | undefined>(
    availableCountries[0]
  );
  const { register, handleSubmit, errors } = useForm<{
    feeMultiplier: number;
  }>();

  // Fallback because of state bug
  useEffect(() => {
    if (!selectedCountry) {
      fetchWithAuth("/settings/countries").then((response: ICountry[]) => {
        setSelectedCountry(response[0]);
      });
    }
  });

  const handleSelectCountry = (event: ChangeEvent<HTMLSelectElement>) => {
    let name = event.target.value.trim();
    let country = availableCountries.find(
      (country: ICountry) => country.name === name
    );
    setSelectedCountry(country);
  };

  return (
    <form
      onSubmit={handleSubmit((data: ICountry) =>
        updateFee({ ...data, id: selectedCountry?.id })
      )}
    >
      <div className="form-group">
        <select
          className="form-control"
          id="destinationCountry"
          name="destinationCountry"
          onChange={handleSelectCountry}
        >
          {availableCountries.map((country: ICountry) => (
            <option key={country.id}>{country.name}</option>
          ))}
        </select>
      </div>
      <div className="form-group row">
        <div className="col">
          <label htmlFor="country">Selected country</label>
          <input
            type="text"
            className="form-control"
            id="country"
            name="country"
            value={selectedCountry?.name}
            readOnly
          />
        </div>
        <div className="col">
          <label htmlFor="feeMultiplier">Fee multiplier</label>
          <input
            type="number"
            step="1"
            className="form-control"
            id="feeMultiplier"
            name="feeMultiplier"
            onChange={(event: ChangeEvent<HTMLInputElement>) => {
              setSelectedCountry({
                ...selectedCountry,
                feeMultiplier: event.target.value,
              });
            }}
            value={selectedCountry?.feeMultiplier}
            ref={register({
              required: true,
              min: 10,
              max: 100,
              pattern: /^\d+(\.\d{1,2})?$/,
            })}
          />
          {errors.feeMultiplier?.type === "min" && (
            <p className="invalid">Minimum fee multiplier is 10</p>
          )}
          {errors.feeMultiplier?.type === "max" && (
            <p className="invalid">Maximum fee multiplier is 100</p>
          )}
        </div>
      </div>
      <button type="submit" className="btn btn-success mt-1">
        Update fee
      </button>
    </form>
  );
};
