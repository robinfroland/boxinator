import React from "react";
import { IShipment } from "../../constants/interfaces";
import { Shipment } from "./Shipment";

interface Props {
  shipments: IShipment[] | null;
  updateShipments?: (shipment: IShipment) => void;
  updateStatus?: (shipment: IShipment) => void;
}

/*
 * A basic component that display every shipment.
 */
export const ShipmentList: React.FC<Props> = ({
  shipments,
  updateShipments,
  updateStatus,
}) => {
  return (
    <ul>
      {shipments?.map((shipment) => (
        <Shipment
          updateShipments={updateShipments}
          updateStatus={updateStatus}
          shipment={shipment}
          key={shipment.id}
        />
      ))}
    </ul>
  );
};
