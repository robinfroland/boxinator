import React from "react";
import { AccountType } from "../../constants/constants";
import { IShipment } from "../../constants/interfaces";
import { ShipmentStatus } from "../../constants/constants";
import { useAppState } from "../../context/context";
import { StatusDropdown } from "./admin/StatusDropdown";
import { ReactComponent as DeleteBtn } from "../../assets/delete_icon.svg";

interface Props {
  shipment: IShipment;
  updateShipments?: (shipment: IShipment) => void;
  updateStatus?: (shipment: IShipment) => void;
}

/*
 * A shipment contains:
 * - the date it was created
 * - the receivers name
 * - the destination country
 * - the box weight
 * - the total price
 * - the box color.
 * The user will also be able to cancel a shipment with an icon on each shipment.
 */

export const Shipment: React.FC<Props> = ({
  shipment,
  updateShipments,
  updateStatus,
}) => {
  const { role } = useAppState();

  const status: any =
    shipment.status === ShipmentStatus.IN_TRANSIT
      ? shipment.status.replace("_", " ")
      : shipment.status;

  return (
    <li className="fluid">
      <div className="container d-flex">
        <div className="col">
          <p className="attribute-title">Status</p>
          {role === AccountType.ADMINISTRATOR ? (
            <StatusDropdown
              status={status}
              shipment={shipment}
              updateStatus={updateStatus}
            />
          ) : (
            <p>{status}</p>
          )}
        </div>
        <div className="col">
          <p className="attribute-title">Created</p>
          <p>{shipment.dateCreated}</p>
        </div>
        <div className="col">
          <p className="attribute-title">Receiver</p>
          <p>{shipment.shipmentPackage.receiverName}</p>
        </div>
        <div className="col">
          <p className="attribute-title">Destination</p>
          <p>{shipment.country.name}</p>
        </div>
        <div className="col">
          <p className="attribute-title">Weight</p>
          <p>{shipment.shipmentPackage.weight} kg</p>
        </div>
        <div className="col">
          <p className="attribute-title">Total price</p>
          <p>{shipment.totalPrice} kr</p>
        </div>
        <div className="col d-flex flex-column">
          <p className="attribute-title">Box color</p>
          <svg height="15" width="60">
            <rect
              x="0"
              y="0"
              height="15"
              width="60"
              rx="7"
              ry="10"
              fill={shipment.shipmentPackage.boxColor}
            />
          </svg>
        </div>
        <div className="d-flex justify-content-end align-items-center">
          {role === AccountType.ADMINISTRATOR ||
          (role === AccountType.REGISTERED_USER &&
            shipment.status !== ShipmentStatus.CANCELLED &&
            shipment.status !== ShipmentStatus.COMPLETED) ? (
            <DeleteBtn
              className="icon-btn delete-shipment"
              onClick={() => updateShipments && updateShipments(shipment)}
            />
          ) : (
            <div className="spacer" />
          )}
        </div>
      </div>
    </li>
  );
};
