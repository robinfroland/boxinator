import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import Modal from "react-modal";
import { useForm } from "react-hook-form";
import { BOX_COLORS } from "../../constants/constants";
import { CirclePicker } from "react-color";
import { ICountry, IShipment } from "../../constants/interfaces";
import { useAppState } from "../../context/context";
import { IShipmentPackage } from "../../constants/interfaces";
import { ReactComponent as CloseButton } from "../../assets/close_icon.svg";

interface Props {
  setColor: Dispatch<SetStateAction<string>>;
  updateShipments: (shipment: IShipment) => void;
}

/*
 * The addShipmentForm is a component being used in all the different dashboards.
 * It is required to add the receiverName and boxWeight. The boxColor is set to Default
 * if the user don't make any changes on the color. The user must also choose what country
 * the package is heading to. This can be done by a dropdown menu containing available countries.
 */
export const AddShipmentForm: React.FC<Props> = ({
  setColor,
  updateShipments,
}) => {
  const { availableCountries } = useAppState();
  const { register, handleSubmit, errors } = useForm<IShipmentPackage>();
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [shipmentPayload, setShipmentPayload] = useState<IShipment>({
    country: {
      id: "",
      name: "",
    },
    shipmentPackage: {
      receiverName: "",
      weight: "",
      boxColor: "#6610f2",
    },
  });

  useEffect(() => {
    Modal.setAppElement("#root");
  }, []);

  const handleAddShipment = async () => {
    updateShipments(shipmentPayload);
    setModalIsOpen(false);
  };

  const onSubmit = (data: any) => {
    setShipmentPayload({
      country: {
        id: availableCountries.find(
          (country: ICountry) => country.name === data.country
        ).id,
        name: data.country,
      },
      shipmentPackage: {
        ...shipmentPayload.shipmentPackage,
        receiverName: data.receiverName,
        weight: data.weight,
      },
    });
    setModalIsOpen(true);
  };

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            id="receiverName"
            name="receiverName"
            data-testid="receiverName"
            aria-invalid={!!errors.receiverName}
            placeholder="Name of recipient"
            ref={register({
              required: true,
              minLength: 2,
              maxLength: 50,
              pattern: /^[ÆØÅæøåa-zA-Z\s]*$/,
            })}
          />
          {errors.receiverName?.type === "required" && (
            <p className="invalid">Recipient name is required</p>
          )}
          {errors.receiverName?.type === "minLength" && (
            <p className="invalid">
              Reciever name must be at least 2 characters
            </p>
          )}
          {errors.receiverName?.type === "maxLength" && (
            <p className="invalid">Receiver name can't exceed 50 characters</p>
          )}
          {errors.receiverName?.type === "pattern" && (
            <p className="invalid">Name can only contain letters</p>
          )}
        </div>
        <div className="form-group">
          <input
            type="number"
            step="0.1"
            className="form-control"
            id="weight"
            name="weight"
            data-testid="weight"
            aria-invalid={!!errors.weight}
            placeholder="Weight"
            ref={register({
              required: true,
              min: 0.1,
              max: 25,
              pattern: /^\d+(\.\d{1,2})?$/,
            })}
          />
          {errors.weight?.type === "required" && (
            <p className="invalid">Package weight is required</p>
          )}
          {errors.weight?.type === "min" && (
            <p className="invalid">Package weight must be at least 100 grams</p>
          )}
          {errors.weight?.type === "max" && (
            <p className="invalid">Package weight can't exceed 25 kg</p>
          )}
          {errors.weight?.type === "pattern" && (
            <p className="invalid">Package weight must be a valid number</p>
          )}
        </div>
        <label>Select box color:</label>
        <CirclePicker
          className="color-picker"
          circleSize={20}
          circleSpacing={8}
          width="100%"
          colors={BOX_COLORS}
          onChange={(color) => {
            setColor(color.hex);
            setShipmentPayload({
              ...shipmentPayload,
              shipmentPackage: {
                ...shipmentPayload.shipmentPackage,
                boxColor: color.hex,
              },
            });
          }}
        />
        <div className="form-group">
          <div className="seperator">
            <label htmlFor="country">Send to:</label>
          </div>
          <select
            className="form-control"
            id="country"
            name="country"
            data-testid="country"
            ref={register({
              required: true,
            })}
          >
            <option value="DEFAULT" disabled>
              Select destination country
            </option>
            {availableCountries.map((country: ICountry) => (
              <option key={country.id}>{country.name}</option>
            ))}
          </select>
        </div>
        <button
          type="submit"
          data-testid="shipBtn"
          className="btn btn-success mt-1"
        >
          Add Shipment
        </button>
        <Modal
          className="Modal"
          overlayClassName="Overlay"
          isOpen={modalIsOpen}
          onRequestClose={() => setModalIsOpen(false)}
        >
          <div className="top d-flex justify-content-end">
            <CloseButton onClick={() => setModalIsOpen(false)} />
          </div>
          <div className="content">
            <div className="header">
              <h4>Shipment overview</h4>
              <p>
                Here is an overview of the shipment you've made. <br /> Please
                make sure everything looks correct before confirming.
              </p>
            </div>
            <p>
              <span className="font-weight-bold">Receiver name: </span>
              {shipmentPayload.shipmentPackage.receiverName}
            </p>
            <p>
              <span className="font-weight-bold">Destination country: </span>
              {
                availableCountries.find(
                  (country: ICountry) =>
                    country.id === shipmentPayload.country.id
                )?.name
              }
            </p>
            <p>
              <span className="font-weight-bold">Package Weight: </span>
              {shipmentPayload.shipmentPackage.weight} kg
            </p>
            <p>
              <span className="font-weight-bold">Box color: </span>
              <svg height="16" width="16">
                <circle
                  cx="50%"
                  cy="50%"
                  r="8"
                  fill={shipmentPayload.shipmentPackage.boxColor}
                />
              </svg>
            </p>
            <div>
              <button
                type="button"
                className="btn btn-success mt-2"
                onClick={handleAddShipment}
              >
                Confirm Shipment
              </button>
            </div>
          </div>
        </Modal>
      </form>
    </div>
  );
};
