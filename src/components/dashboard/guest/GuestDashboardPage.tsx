import React, { useState } from "react";
import { IShipment } from "../../../constants/interfaces";
import { postWithAuth } from "../../../services/ApiService";
import { AddShipmentForm } from "../AddShipmentForm";
import { PackageIllustration } from "../user/PackageIllustration";
import { Alert } from "react-bootstrap";

/*
 * The GuestDashboard is the most simple dashboard in this application.
 * It does not contain any list of active/previous shipments as there are
 * no shipments linked to a guest user. A guest will only have the opportunity
 * to add new shipments. The AddShipmentForm is the same for every user.
 */
export const GuestDashboardPage: React.FC = () => {
  const [color, setColor] = useState("#6610f2");
  const [showAlert, setShowAlert] = useState(false);

  const addShipment = async (shipment: IShipment) => {
    await postWithAuth("/shipments", shipment);
    setShowAlert(true);
  };
  return (
    <>
      <div className="container d-flex justify-content-center">
        <Alert
          show={showAlert}
          variant="success"
          className="text-center position-absolute"
          onClose={() => setShowAlert(false)}
          dismissible
        >
          Shipment successfully added!
        </Alert>
      </div>
      <div className="dashboard">
        <section className="container add-shipment">
          <div className="row">
            <div className="col">
              <h3>Add new shipment</h3>
              <AddShipmentForm
                updateShipments={addShipment}
                setColor={setColor}
              />
            </div>
            <div className="d-none d-md-flex col-md-7 justify-content-center">
              <PackageIllustration color={color} />
            </div>
          </div>
        </section>
      </div>
    </>
  );
};
