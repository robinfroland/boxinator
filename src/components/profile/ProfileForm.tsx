import React, { useState } from "react";
import { DeepMap, useForm } from "react-hook-form";
import { COUNTRIES } from "../../constants/constants";
import { IAccount } from "../../constants/interfaces";
import { ReactComponent as EditBtn } from "../../assets/edit_icon.svg";

interface Props {
  account: IAccount | null;
  updateProfile: (
    account: IAccount,
    dirtyFields: DeepMap<IAccount, true>
  ) => void;
}

export const ProfileForm: React.FC<Props> = ({ account, updateProfile }) => {
  const { register, handleSubmit, errors, formState, watch } = useForm<
    IAccount
  >();
  const { dirtyFields } = formState;
  const [isEditing, setIsEditing] = useState(false);

  return (
    <form
      onSubmit={handleSubmit((data: IAccount) => {
        updateProfile(data, dirtyFields);
        setIsEditing(false);
      })}
    >
      <div className="d-flex justify-content-between align-items-center mt-4 mb-3">
        <h5>Profile information</h5>
        <div className="edit-btn" onClick={() => setIsEditing(!isEditing)}>
          <p className="d-inline">Edit profile</p>
          <EditBtn data-testid="EditBtn" className="icon-btn d-inline" />
        </div>
      </div>
      <div className="form-group">
        <label htmlFor="email">E-mail address</label>
        <input
          type="text"
          className="form-control"
          id="email"
          name="email"
          readOnly
          defaultValue={account?.email}
          ref={register({
            required: true,
            // Regex: General Email Regex (RFC 5322 Official Standard)
            pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          })}
        />
      </div>
      <div className="row">
        <div className="form-group col">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            className="form-control"
            id="password"
            name="password"
            disabled={!isEditing}
            aria-invalid={!!errors.password}
            placeholder="********"
            ref={register({
              minLength: 8,
              // Regex: Ensure string has at least one uppercase letters and at least one special character
              pattern: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/,
            })}
          />
          {errors.password?.type === "minLength" && (
            <p className="invalid">
              Password must be at least 8 characters long
            </p>
          )}
          {errors.password?.type === "pattern" && (
            <p className="invalid">
              Include at least one special character and number
            </p>
          )}
        </div>
        {isEditing && (
          <div className="form-group col-md-6 col-12">
            <label htmlFor="passwordConfirm">Confirm password</label>
            <input
              type="password"
              className="form-control"
              id="passwordConfirm"
              name="passwordConfirm"
              aria-invalid={!!errors.passwordConfirm}
              placeholder="********"
              ref={register({
                validate: (value) =>
                  value === watch("password") || "Passwords don't match",
              })}
            />
            {errors.password?.type === "required" && (
              <p className="invalid">Please confirm password</p>
            )}
            {errors.passwordConfirm && (
              <p className="invalid">{errors.passwordConfirm?.message}</p>
            )}
          </div>
        )}
      </div>
      <div className="row">
        <div className="form-group col-md-6 col-12">
          <label htmlFor="firstName">First name</label>
          <input
            type="text"
            id="firstName"
            name="firstName"
            className="form-control"
            disabled={!isEditing}
            defaultValue={account?.firstName}
            ref={register({
              required: true,
              minLength: 2,
              maxLength: 50,
              pattern: /^[ÆØÅæøåa-zA-Z\s]*$/,
            })}
          />
          {errors.firstName?.type === "required" && (
            <p className="invalid">First name is required</p>
          )}
          {errors.firstName?.type === "minLength" && (
            <p className="invalid">First name must be at least 2 characters</p>
          )}
          {errors.firstName?.type === "maxLength" && (
            <p className="invalid">First name can't exceed 50 characters</p>
          )}
          {errors.firstName?.type === "pattern" && (
            <p className="invalid">Name can only contain letters</p>
          )}
        </div>
        <div className="form-group col-md-6 col-12">
          <label htmlFor="lastName">Last name</label>
          <input
            type="text"
            className="form-control"
            id="lastName"
            name="lastName"
            disabled={!isEditing}
            defaultValue={account?.lastName}
            ref={register({
              required: true,
              minLength: 2,
              maxLength: 50,
              pattern: /^[ÆØÅæøåa-zA-Z\s]*$/,
            })}
          />
          {errors.lastName?.type === "required" && (
            <p className="invalid">Last name is required</p>
          )}
          {errors.lastName?.type === "minLength" && (
            <p className="invalid">Last name must be at least 2 characters</p>
          )}
          {errors.lastName?.type === "maxLength" && (
            <p className="invalid">Last name can't exceed 50 characters</p>
          )}
          {errors.lastName?.type === "pattern" && (
            <p className="invalid">Name can only contain letters</p>
          )}
        </div>
      </div>
      <div className="row">
        <div className="form-group col-md-6 col-12">
          <label htmlFor="phone">Phone number</label>
          <input
            type="tel"
            className="form-control"
            id="phone"
            name="phone"
            disabled={!isEditing}
            defaultValue={account?.phone}
            ref={register({
              minLength: 8,
              maxLength: 8,
              pattern: /^(\d)*$/,
            })}
          />
          {(errors.phone?.type === "minLength" ||
            errors.phone?.type === "maxLength") && (
            <p className="invalid">Phone number must be 8 digits long</p>
          )}
          {errors.phone?.type === "pattern" && (
            <p className="invalid">Phone number can only contain digits</p>
          )}
        </div>
        <div className="form-group col-md-6 col-12">
          <label htmlFor="dob">Date of birth</label>
          <input
            className="form-control"
            type="date"
            id="dob"
            name="dob"
            disabled={!isEditing}
            defaultValue={account?.dob}
            ref={register()}
          />
        </div>
      </div>
      <div className="row">
        <div className="form-group col-md-6 col-12">
          <label htmlFor="country">Country</label>
          <select
            id="country"
            name="country"
            className="form-control"
            disabled={!isEditing}
            defaultValue={account?.country}
            ref={register()}
          >
            <option>{account?.country}</option>
            {COUNTRIES.map((country) => (
              <option key={country.toString()}>{country}</option>
            ))}
          </select>
        </div>
        <div className="form-group col-md-6 col-12">
          <label htmlFor="zipCode">Zip code</label>
          <input
            type="text"
            className="form-control"
            id="zipCode"
            name="zipCode"
            disabled={!isEditing}
            defaultValue={account?.zipCode}
            ref={register({
              minLength: 4,
              maxLength: 6,
              pattern: /^(\d)*$/,
            })}
          />
          {errors.zipCode?.type === "minLength" && (
            <p className="invalid">Zip code must be at least 4 character</p>
          )}
          {errors.zipCode?.type === "maxLength" && (
            <p className="invalid">Zip code can't exceed 6 character</p>
          )}
          {errors.zipCode?.type === "pattern" && (
            <p className="invalid">Zip code can only contain digits</p>
          )}
        </div>
      </div>
      {isEditing && (
        <button type="submit" className="btn btn-primary btn-lg mt-3">
          Save Changes
        </button>
      )}
    </form>
  );
};
