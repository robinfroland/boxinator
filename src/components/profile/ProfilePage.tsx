import React, { useEffect, useRef, useState } from "react";
import { IAccount } from "../../constants/interfaces";
import { ProfileForm } from "./ProfileForm";
import { useHistory } from "react-router";
import { logout } from "../../context/actions";
import { useAppDispatch, useAppState } from "../../context/context";
import { fetchWithAuth, putWithAuth } from "../../services/ApiService";
import { AccountType, Actions } from "../../constants/constants";
import { DeepMap } from "react-hook-form";
import { Alert } from "react-bootstrap";

interface Props {
  account: IAccount;
}

/*
 * The account page component doesn't do too much.
 * If a guest access the page, the only feature will be to sign out.
 * If a registered user access this page, the profile form with all the
 * data will be displayed, as well as a sign out button.
 */
export const ProfilePage: React.FC<Props> = () => {
  const [profile, setProfile] = useState<IAccount | null>(null);
  const dispatch = useAppDispatch();
  const { name, role } = useAppState();
  const history = useHistory();
  const isMounted = useRef<boolean>(true);
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    fetchWithAuth("/account")
      .then((response) => {
        if (isMounted.current) {
          setProfile(response);
        }
      })
      .catch((error) => console.log(error));
    return () => {
      isMounted.current = false;
    };
  }, [isMounted]);

  const updateProfile = async (
    account: IAccount,
    dirtyFields: DeepMap<IAccount, true>
  ) => {
    await putWithAuth(
      "/account",
      dirtyFields.password ? account : { ...account, password: undefined }
    );
    if (dirtyFields.firstName || dirtyFields.lastName) {
      dispatch({
        type: Actions.UPDATE_PROFILE,
        payload: account.firstName + " " + account.lastName,
      });
    }
    setShowAlert(true);
  };

  return (
    <>
      <div className="container d-flex justify-content-center">
        <Alert
          show={showAlert}
          variant="success"
          className="text-center position-absolute"
          onClose={() => setShowAlert(false)}
          dismissible
        >
          Profile successfully updated!
        </Alert>
      </div>
      <div className="profile container d-flex flex-column">
        <div className="w-100 d-flex justify-content-between align-items-center">
          {role !== AccountType.GUEST ? (
            <h1>Hi, {name}</h1>
          ) : (
            <h1>Guest session</h1>
          )}

          <div>
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => {
                logout(dispatch);
                history.push("/login");
              }}
            >
              Log out
            </button>
          </div>
        </div>
        <hr />
        {role !== AccountType.GUEST && (
          <div className="d-flex justify-content-center">
            <ProfileForm account={profile} updateProfile={updateProfile} />
          </div>
        )}
      </div>
    </>
  );
};
