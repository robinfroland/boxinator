import React from "react";
import { useForm } from "react-hook-form";
import { COUNTRIES } from "../../constants/constants";
import { IAccount } from "../../constants/interfaces";

interface Props {
  onRegistration: (data: IAccount) => void;
  guestEmail?: string;
}

/*
 * This component contains the form that appears on the register page.
 * Here the user can fill in the required fields to register at boxinator.
 * Only email, password, confirm password, first name and last name are required.
 * The other fields are optional and can be filled if the user wants to.
 * There are checks to check that all the required fields are filled in,
 * and that the inputs are being valid.
 */
export const RegisterForm: React.FC<Props> = ({
  onRegistration,
  guestEmail,
}) => {
  const { register, handleSubmit, watch, errors } = useForm<IAccount>();

  return (
    <form onSubmit={handleSubmit((data: IAccount) => onRegistration(data))}>
      <div className="form-group">
        <label htmlFor="email">E-mail address*</label>
        <input
          type="text"
          className="form-control"
          id="email"
          defaultValue={guestEmail}
          disabled={guestEmail ? true : false}
          name="email"
          aria-invalid={!!errors.email}
          placeholder="Enter your e-mail address"
          ref={register({
            required: true,
            // Regex: General Email Regex (RFC 5322 Official Standard)
            pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          })}
        />
        {errors.email?.type === "required" && (
          <p className="invalid">E-mail is required</p>
        )}
        {errors.email?.type === "pattern" && (
          <p className="invalid">Please enter a valid email address</p>
        )}
      </div>
      <div className="row">
        <div className="form-group col-md-6 col-12">
          <label htmlFor="password">Password*</label>
          <input
            type="password"
            className="form-control"
            id="password"
            name="password"
            aria-invalid={!!errors.password}
            placeholder="Enter your password"
            ref={register({
              required: true,
              minLength: 8,
              // Regex: Ensure string has at least one uppercase letters and at least one special character
              pattern: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/,
            })}
          />
          {errors.password?.type === "required" && (
            <p className="invalid">Password is required</p>
          )}
          {errors.password?.type === "minLength" && (
            <p className="invalid">
              Password must be at least 8 characters long
            </p>
          )}
          {errors.password?.type === "pattern" && (
            <p className="invalid">
              Include at least one special character and number
            </p>
          )}
        </div>
        <div className="form-group col-md-6 col-12">
          <label htmlFor="passwordConfirm">Confirm password*</label>
          <input
            type="password"
            className="form-control"
            id="passwordConfirm"
            name="passwordConfirm"
            placeholder="Confirm your password"
            ref={register({
              required: true,
              validate: (value) =>
                value === watch("password") || "Passwords don't match",
            })}
          />
          {errors.password?.type === "required" && (
            <p className="invalid">Please confirm password</p>
          )}
          {errors.passwordConfirm && (
            <p className="invalid">{errors.passwordConfirm?.message}</p>
          )}
        </div>
      </div>
      <div className="row">
        <div className="form-group col-md-6 col-12">
          <label htmlFor="firstName">First name*</label>
          <input
            type="text"
            id="firstName"
            name="firstName"
            className="form-control"
            placeholder="Enter your first name"
            ref={register({
              required: true,
              minLength: 2,
              maxLength: 50,
              pattern: /^[ÆØÅæøåa-zA-Z\s]*$/,
            })}
          />
          {errors.firstName?.type === "required" && (
            <p className="invalid">First name is required</p>
          )}
          {errors.firstName?.type === "minLength" && (
            <p className="invalid">First name must be at least 2 characters</p>
          )}
          {errors.firstName?.type === "maxLength" && (
            <p className="invalid">First name can't exceed 50 characters</p>
          )}
          {errors.firstName?.type === "pattern" && (
            <p className="invalid">Name can only contain letters</p>
          )}
        </div>
        <div className="form-group col-md-6 col-12">
          <label htmlFor="lastName">Last name*</label>
          <input
            type="text"
            className="form-control"
            id="lastName"
            name="lastName"
            placeholder="Enter your last name"
            ref={register({
              required: true,
              minLength: 2,
              maxLength: 50,
              pattern: /^[ÆØÅæøåa-zA-Z\s]*$/,
            })}
          />
          {errors.lastName?.type === "required" && (
            <p className="invalid">Last name is required</p>
          )}
          {errors.lastName?.type === "minLength" && (
            <p className="invalid">Last name must be at least 2 characters</p>
          )}
          {errors.lastName?.type === "maxLength" && (
            <p className="invalid">Last name can't exceed 50 characters</p>
          )}
          {errors.lastName?.type === "pattern" && (
            <p className="invalid">Name can only contain letters</p>
          )}
        </div>
      </div>
      <div className="row">
        <div className="form-group col-md-6 col-12">
          <label htmlFor="phone">Phone number</label>
          <input
            type="tel"
            className="form-control"
            id="phone"
            name="phone"
            placeholder="Enter your phone number"
            ref={register({
              minLength: 8,
              maxLength: 8,
              pattern: /^(\d)*$/,
            })}
          />
          {(errors.phone?.type === "minLength" ||
            errors.phone?.type === "maxLength") && (
            <p className="invalid">Phone number must be 8 digits long</p>
          )}
          {errors.phone?.type === "pattern" && (
            <p className="invalid">Phone number can only contain digits</p>
          )}
        </div>
        <div className="form-group col-md-6 col-12">
          <label htmlFor="dob">Date of birth</label>
          <input
            className="form-control"
            type="date"
            id="dob"
            name="dob"
            ref={register()}
          />
        </div>
      </div>
      <div className="row">
        <div className="form-group col-md-6 col-12">
          <label htmlFor="country">Country</label>
          <select
            id="country"
            name="country"
            className="form-control"
            ref={register()}
          >
            <option value="DEFAULT" disabled>
              Select country of residence
            </option>
            {COUNTRIES.map((country) => (
              <option key={country.toString()}>{country}</option>
            ))}
          </select>
        </div>
        <div className="form-group col-md-6 col-12">
          <label htmlFor="zipCode">Zip code</label>
          <input
            type="text"
            className="form-control"
            id="zipCode"
            name="zipCode"
            placeholder="Enter your zip code"
            ref={register({
              minLength: 4,
              maxLength: 6,
              pattern: /^(\d)*$/,
            })}
          />
          {errors.zipCode?.type === "minLength" && (
            <p className="invalid">Zip code must be at least 4 character</p>
          )}
          {errors.zipCode?.type === "maxLength" && (
            <p className="invalid">Zip code can't exceed 6 character</p>
          )}
          {errors.zipCode?.type === "pattern" && (
            <p className="invalid">Zip code can only contain digits</p>
          )}
        </div>
      </div>

      <button type="submit" className="btn btn-primary btn-lg mt-3">
        Register account
      </button>
    </form>
  );
};
