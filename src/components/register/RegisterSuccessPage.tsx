import React from "react";
import { Link } from "react-router-dom";

/*
 * After registering you will be redirected to this page.
 * It only contains some text and a button.
 */
export const RegisterSuccessPage: React.FC<string> = (email: string) => {
  return (
    <div className="container mt-5 d-flex flex-column align-items-center top-container">
      <h2>Registration success</h2>
      <p>
        Please check your email for instructions on how to activate your
        account.
      </p>

      <Link to="/login">
        <button className="btn btn-primary">Go to login</button>
      </Link>
    </div>
  );
};
