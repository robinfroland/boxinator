import React, { useEffect, useRef, useState } from "react";
import { useHistory, useLocation } from "react-router";
import { Link } from "react-router-dom";
import { BASE_URL } from "../../constants/constants";

/*
 * This is the page you get redirected to after claiming an account.
 * It will let you proceed to login if the token exists and is valid.
 * If not you will get a message saying otherwise.
 */
export const VerifyPage: React.FC = () => {
  const [status, setStatus] = useState(0);
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const isMounted = useRef<boolean>(true);
  const token: string | null = new URLSearchParams(useLocation().search).get(
    "token"
  );

  useEffect(() => {
    if (token === null) {
      history.push("/login");
    }
    const fetchData = async () => {
      await fetch(BASE_URL + "/account/verify?token=" + token, {
        method: "PUT",
      }).then((response) => {
        if (isMounted.current) {
          setStatus(response.status);
          setLoading(false);
        }
      });
    };
    fetchData();
    return () => {
      isMounted.current = false;
    };
  }, [token, history]);

  return (
    <div className="top-container">
      {loading ? (
        <div className="text-center mt-5">Verifying account...</div>
      ) : (
        <div className="container mt-5 d-flex flex-column align-items-center">
          {status === 200 ? (
            <>
              <h2>Account sucessfully activated!</h2>
              <p>Click the button below to log in to Boxinator</p>
              <Link to="/login">
                <button className="btn btn-primary">Go to login</button>
              </Link>
            </>
          ) : (
            <>
              <p>
                The provided verification token is either expired or invalid
              </p>
              <p>HTTP status: {status}</p>
            </>
          )}
        </div>
      )}
    </div>
  );
};
