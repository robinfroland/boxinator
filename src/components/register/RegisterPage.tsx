import React, { useEffect, useRef, useState } from "react";
import { useHistory, useLocation } from "react-router";
import { AccountType, BASE_URL } from "../../constants/constants";
import { IAccount } from "../../constants/interfaces";
import { fetchWithAuth } from "../../services/ApiService";
import { RegisterForm } from "./RegisterForm";
import Modal from "react-modal";

/*
 * The Register page mainly contains the register form which mainly does the checks.
 * The page will look different if you are a guest that is regestering, or an user.
 */
export const RegisterPage: React.FC = () => {
  const [errorStatus, setErrorStatus] = useState<number | null>(null);
  const [guestEmail, setGuestEmail] = useState<string | undefined>(undefined);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [TwoFACode, setTwoFACode] = useState<any>("");
  const history = useHistory();
  const isMounted = useRef<boolean>(true);
  const token: string | null = new URLSearchParams(useLocation().search).get(
    "token"
  );

  useEffect(() => {
    // If guest registration (token included in URL)
    if (token !== null) {
      localStorage.clear();
      fetchWithAuth(`/account?token=${token}`).then((response: IAccount) => {
        if (isMounted.current) {
          setGuestEmail(response.email);
        }
      });
    }
    return () => {
      isMounted.current = false;
    };
  }, [token]);

  useEffect(() => {
    Modal.setAppElement("#root");
  }, []);

  const handleOnSubmit = async (data: IAccount) => {
    data.role = guestEmail ? AccountType.GUEST : AccountType.REGISTERED_USER;
    await fetch(BASE_URL + "/account", {
      method: guestEmail ? "PUT" : "POST",
      headers: { "Content-Type": "application/json" },
      body: guestEmail
        ? JSON.stringify({ ...data, email: guestEmail })
        : JSON.stringify(data),
    }).then((response) => {
      if (response.ok) {
        fetchTwoFA(guestEmail ? guestEmail : data.email);
      } else {
        setErrorStatus(response.status);
      }
    });
  };

  const fetchTwoFA = async (email: string) => {
    const requestOptions: RequestInit = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: email }),
    };

    await fetch(`${BASE_URL}/2fa/generate/`, requestOptions)
      .then((response) => response.json())
      .then((response) => {
        setTwoFACode(response.url);
      });
    setModalIsOpen(true);
  };

  return (
    <div className="container d-flex flex-column align-items-center top-container">
      {!token ? (
        <h1 className="mb-5">Register an account</h1>
      ) : (
        <h1>Complete guest registration</h1>
      )}

      {errorStatus === 409 && (
        <p className="text-danger">
          An account with this email address is already registered.
        </p>
      )}
      {errorStatus === 400 && (
        <p className="text-danger">
          Something went wrong. Please make sure your submitted information
          looks correct.
        </p>
      )}
      <RegisterForm onRegistration={handleOnSubmit} guestEmail={guestEmail} />
      <Modal className="Modal" overlayClassName="Overlay" isOpen={modalIsOpen}>
        <div className="content">
          <div className="header">
            <h4>Two-factor Authentication</h4>
            <p>
              To use Boxinator, you first have to enable two-factor
              authentication. Simply scan this QR-code with your favorite
              authenticator app. You cannot retrieve this code at a later stage,
              so it is important to do this now, or you will lose access to your
              account.
            </p>
            <img
              style={{ height: "200px", width: "200px" }}
              src={TwoFACode}
              alt="2FA QR-code"
            />
          </div>
          <div className="d-flex justify-content-center">
            <button
              type="submit"
              className="btn btn-success"
              onClick={() => history.push("/register/success")}
            >
              Complete Registration
            </button>
            <button
              type="submit"
              className="btn btn-secondary ml-2"
              onClick={() => setModalIsOpen(false)}
            >
              Cancel
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
};
