import React from "react";
import { Route, Redirect } from "react-router-dom";
import { AccountType } from "../../constants/constants";
import { useAppState } from "../../context/context";
import { dashboardRouteSwitch } from "../../services/RouteService";

interface Props {
  component: any;
  exact: any;
  path: string | string[] | undefined;
}

export const AdminProtectedRoute: React.FC<Props> = ({
  component: Component,
  ...rest
}) => {
  const { role, isAuthenticated } = useAppState();

  return (
    <Route
      {...rest}
      render={(props) => {
        if (isAuthenticated) {
          if (role === AccountType.ADMINISTRATOR) {
            return <Component {...props} />;
          } else {
            return dashboardRouteSwitch(role);
          }
        } else {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
};
