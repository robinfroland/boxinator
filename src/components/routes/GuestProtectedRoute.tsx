import React from "react";
import { Route, Redirect } from "react-router-dom";
import { AccountType } from "../../constants/constants";
import { useAppState } from "../../context/context";
import { dashboardRouteSwitch } from "../../services/RouteService";

interface Props {
  component: any;
  exact: any;
  path: string | string[] | undefined;
}

export const GuestProtectedRoute: React.FC<Props> = ({
  component: Component,
  ...rest
}) => {
  const { role } = useAppState();

  return (
    <Route
      {...rest}
      render={(props) => {
        if (role === AccountType.GUEST) {
          return <Component {...props} />;
        } else if (role === null) {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location,
                },
              }}
            />
          );
        } else {
          return dashboardRouteSwitch(role);
        }
      }}
    />
  );
};
