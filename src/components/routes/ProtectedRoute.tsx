import React from "react";
import { Route, Redirect } from "react-router-dom";
import { AccountType } from "../../constants/constants";
import { useAppState } from "../../context/context";

interface Props {
  component: any;
  exact: any;
  path: string;
}

export const ProtectedRoute: React.FC<Props> = ({
  component: Component,
  path,
  ...rest
}) => {
  const { isAuthenticated, role } = useAppState();

  return (
    <Route
      {...rest}
      render={(props) => {
        if (isAuthenticated || role === AccountType.GUEST) {
          return <Component {...props} />;
        } else {
          return <Redirect to="/login" />;
        }
      }}
    />
  );
};
