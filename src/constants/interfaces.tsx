import { AccountType } from "./constants";

/* Package Interface */
export interface IShipmentPackage {
  weight: string | number;
  receiverName: string;
  boxColor?: string;
}

/* Shipment Interface */
export interface IShipment {
  id?: string | number;
  status?: string;
  dateCreated?: string;
  totalPrice?: string | number;
  shipmentPackage: IShipmentPackage;
  account?: {
    id?: string;
  };
  country: {
    name?: string;
    id: string;
  };
}

/* Account Interface */
export interface IAccount {
  id: string | number;
  role: AccountType;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
  zipCode: string | number;
  dob: string;
  country: string;
  // For validation purposes
  passwordConfirm?: string;
}

/* Country Interface */
export interface ICountry {
  id?: string;
  name?: string;
  feeMultiplier: string | number;
}
