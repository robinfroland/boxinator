import {
  ACCOUNT_ROLE_KEY,
  AUTH_TOKEN_KEY,
  ACCOUNT_NAME_KEY,
  Actions,
  COUNTRIES_KEY,
} from "../constants/constants";
import { ICountry } from "../constants/interfaces";
import { getStorage } from "../services/StorageService";

/* APP STATE REDUCER */

let token: string = getStorage(AUTH_TOKEN_KEY);
let role: string = getStorage(ACCOUNT_ROLE_KEY);
let name: string = getStorage(ACCOUNT_NAME_KEY);
let countries: ICountry[] = getStorage(COUNTRIES_KEY);

interface appState {
  isAuthenticated: boolean;
  role: string | null;
  name: string | null;
  loading: boolean;
  loginError: number | null;
  availableCountries: ICountry[] | [];
}
export const initialAppState: appState = {
  isAuthenticated: token !== null,
  role: role || null,
  name: name || null,
  loading: false,
  loginError: null,
  availableCountries: countries || [],
};

/* Return new state based on dispatched action */
export const appStateReducer = (initialState: appState, action: any) => {
  switch (action.type) {
    case Actions.LOGIN_REQUEST:
      return {
        ...initialState,
        loading: true,
      };
    case Actions.LOGIN_SUCCESS:
      return {
        ...initialState,
        role: action.payload.role,
        name: action.payload.name,
        loading: false,
      };
    case Actions.LOGIN_FAILED:
      return {
        ...initialState,
        loginError: action.error,
        loading: false,
      };
    case Actions.LOGOUT:
      return {
        ...initialState,
        role: null,
        name: null,
        isAuthenticated: false,
      };
    case Actions.FETCH_COUNTRIES:
      return {
        ...initialState,
        availableCountries: action.payload,
      };
    case Actions.FETCH_COUNTRIES_FAILED:
      return {
        ...initialState,
        availableCountries: action.payload,
      };
    case Actions.UPDATE_PROFILE:
      return {
        ...initialState,
        name: action.payload,
      };
    case Actions.VALIDATE_TWO_FA:
      return {
        ...initialState,
        isAuthenticated: action.payload,
      };
    default:
      throw new Error(`Unknown action: ${action.type}`);
  }
};
