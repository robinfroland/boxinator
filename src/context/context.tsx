import React, { useReducer } from "react";
import { appStateReducer, initialAppState } from "./reducers";

/* Auth context */

// Default value null because provider will be provided;)
export const AuthStateContext: React.Context<any> = React.createContext(null);
export const AuthDispatchContext: React.Context<any> = React.createContext(
  null
);

/* Custom Hooks for consuming auth context */

export const useAppState = () => {
  return React.useContext(AuthStateContext);
};

export const useAppDispatch = () => {
  return React.useContext(AuthDispatchContext);
};

/* Auth provider */
export const AppStateProvider = ({ children }: any) => {
  const [account, dispatch] = useReducer(appStateReducer, initialAppState);

  return (
    <AuthStateContext.Provider value={account}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
};
