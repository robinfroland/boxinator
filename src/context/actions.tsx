import {
  ACCOUNT_ROLE_KEY,
  AUTH_TOKEN_KEY,
  BASE_URL,
  Actions,
  COUNTRIES_KEY,
  ACCOUNT_NAME_KEY,
  AccountType,
} from "../constants/constants";
import { ICountry } from "../constants/interfaces";
import { fetchWithAuth } from "../services/ApiService";
import { setStorage } from "../services/StorageService";

export const login = async (
  dispatch: any,
  email: string,
  password?: string
): Promise<any> => {
  const requestOptions: RequestInit = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(
      password ? { email: email, password: password } : { email: email }
    ),
  };

  dispatch({ type: Actions.LOGIN_REQUEST });

  return await fetch(BASE_URL + "/login", requestOptions)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw response.status;
      }
    })
    .then((response) => {
      const { role, name, token } = response;
      setStorage(ACCOUNT_ROLE_KEY, role);
      setStorage(ACCOUNT_NAME_KEY, name);
      if (role === AccountType.GUEST) {
        setStorage(AUTH_TOKEN_KEY, token);
        fetchCountries(dispatch);
      }
      dispatch({
        type: Actions.LOGIN_SUCCESS,
        payload: response,
      });
      return response;
    })
    .catch((error) => {
      dispatch({ type: Actions.LOGIN_FAILED, error: error });
      dispatch({ type: Actions.FETCH_COUNTRIES_FAILED, payload: [] });
      return error;
    });
};

export const validateTwoFA = async (
  dispatch: any,
  twoFA: {},
  token: string
) => {
  const requestOptions: RequestInit = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(twoFA),
  };

  return await fetch(BASE_URL + "/2fa/validate", requestOptions)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        dispatch({ type: Actions.VALIDATE_TWO_FA, payload: false });
      }
    })
    .then((response) => {
      const { valid } = response;
      if (valid) {
        setStorage(AUTH_TOKEN_KEY, token);
        fetchCountries(dispatch);
      }
      dispatch({ type: Actions.VALIDATE_TWO_FA, payload: valid });
      return valid;
    });
};

export const fetchCountries = async (dispatch: any) => {
  return await fetchWithAuth("/settings/countries").then(
    (response: ICountry[]) => {
      setStorage(COUNTRIES_KEY, response);
      dispatch({ type: Actions.FETCH_COUNTRIES, payload: response });
      return response;
    }
  );
};

export const logout = async (dispatch: any) => {
  localStorage.clear();
  dispatch({ type: Actions.LOGOUT });
};
