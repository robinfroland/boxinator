# Boxinator frontend

With Boxinator you can create orders for packages to ship around the world. As a registered user you can see all your active and previous shipments, including details about the shipments. An administrator will have access to every users shipments and can manage this on the administrator page. If you don’t want to create an account to send shipments, you can use Boxinator as a guest. You will then receive a receipt on the email you provide when sending the shipment.

If you want to have a look at the backend repo for this project head to [BoxinatorDB](https://gitlab.com/robinfroland/boxinator-db).

## Deployed application

Deployed at https://boxinator-app.herokuapp.com/.

## Technology stack

- [React v17.0.1](https://reactjs.org/) - A JavaScript library for building user interfaces
- [TypeScript 4.0](https://www.typescriptlang.org/) - TypeScript extends JavaScript by adding types
- [Bootstrap](https://getbootstrap.com/) - A free and open-source CSS framework
- [Sass](https://sass-lang.com/) - Syntactically Awesome Style Sheets
- [Heroku](https://dashboard.heroku.com/) - Cloud application platform
- [git](https://git-scm.com/) - Free and Open-Source distributed version control system

## Usage

We recommend to download an IDE like [Visual Studio Code](https://code.visualstudio.com/) for building and running this application.

1. Clone or download the source-code.
2. Open project in VScode.
3. "npm install" the project.
4. "npm run" will run the app in the development mode.
5. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## User Manual

If you want to have a look at the user manual please [click here](Boxinator-User_manual.pdf).

## Team members

Robin Frøland \
Pia Wold Nilsen \
Magne Kjellesvik \
Kristian Gyene
